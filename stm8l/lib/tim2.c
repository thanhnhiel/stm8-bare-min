#include <stm8l.h>
#include "tim2.h"
#include "gpio.h"

void SetOnPWM1(uint16_t t)
{
    /* Set the Capture Compare1 Register value */
    //TIM2_SetCompare1(t); // 0.5mS 
    TIM2_CCR1H = (uint8_t)(t >> 8);
    TIM2_CCR1L = (uint8_t)(t);
    
    // TIM2_OCMode_PWM1      = ((uint8_t)0x60),   /*!< PWM Mode 1*/
    //TIM2_SelectOCxM(TIM2_Channel_1, TIM2_OCMode_PWM1);
    /* Disable the Channel 1: Reset the CCE Bit */
    TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC1E));
    /* Reset the Output Compare Bits */
    TIM2_CCMR1 &= (uint8_t)(~(TIM_CCMR_OCM));
    /* Set the Output Compare Mode */
    TIM2_CCMR1 |= (uint8_t)0x60; // TIM2_OCMode_PWM1
        
    //TIM2_CCxCmd(TIM2_Channel_1, ENABLE);
    /* Set or Reset the CC1E Bit */
    TIM2_CCER1 |= (1<<TIM_CCER1_CC1E);
}

void SetOffPWM1(void)
{
    //TIM2_CCxCmd(TIM2_Channel_1, DISABLE);
    TIM2_CCER1 &= ~(1<<TIM_CCER1_CC1E);
    //GPIO_ResetBits(GPIOD, GPIO_Pin_0);
    //PD_ODR &= (uint8_t)(~(1<<PIN0));
    PinLow(PORTD, PIN0);
}


void SetOnPWM2(uint16_t t)
{
    //TIM2_SetCompare2(t); // 0.5mS 
    /* Set the Capture Compare2 Register value */
    TIM2_CCR2H = (uint8_t)(t >> 8);
    TIM2_CCR2L = (uint8_t)(t);    
   
    //TIM2_SelectOCxM(TIM2_Channel_2, TIM2_OCMode_PWM1);
    /* Disable the Channel 2: Reset the CCE Bit */
    TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC2E));
    /* Reset the Output Compare Bits */
    TIM2_CCMR2 &= (uint8_t)(~TIM_CCMR_OCM);
    /* Set the Output Compare Mode */
    TIM2_CCMR2 |= (uint8_t)0x60; // TIM2_OCMode_PWM1   
    
    //TIM2_CCxCmd(TIM2_Channel_2, ENABLE);
    TIM2_CCER1 |= (1<<TIM_CCER1_CC2E);
}



void SetOffPWM2(void)
{
    //TIM2_CCxCmd(TIM2_Channel_2, DISABLE);
    TIM2_CCER1 &= ~(1<<TIM_CCER1_CC2E);
    
    //GPIO_ResetBits(GPIOB, GPIO_Pin_1);
    //PB_ODR &= (uint8_t)(~(1<<PIN1));
    PinLow(PORTB, PIN1);
}

                 
void tmr2_OC1Init(uint16_t TIM2_Pulse)
{
  uint8_t tmpccmr1 = 0;
  
  tmpccmr1 = TIM2_CCMR1;

  /* Disable the Channel 1: Reset the CCE Bit */
  TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC1E));
  /* Reset the Output Compare Bits */
  tmpccmr1 &= (uint8_t)(~TIM_CCMR_OCM);
  /* Set the Output Compare Mode */
  tmpccmr1 |= (uint8_t)0x60; //TIM2_OCMode_PWM1;
  TIM2_CCMR1 = tmpccmr1;

  /* Set the Output State */
  // TIM2_OutputState_Enable)
  TIM2_CCER1 |= (1<<TIM_CCER1_CC1E);

  /* Set the Output Polarity */
  // TIM2_OCPolarity_High
  TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC1P));


  /* Set the Output Idle state */
  // TIM2_OCIdleState_Set)
  TIM2_OISR |= (1<<TIM_OISR_OIS1);
  
  /* Set the Pulse value */
  TIM2_CCR1H = (uint8_t)(TIM2_Pulse >> 8);
  TIM2_CCR1L = (uint8_t)(TIM2_Pulse);    
}


void tmr2_OC2Init(uint16_t TIM2_Pulse)
{
  uint8_t tmpccmr2 = 0;

  tmpccmr2 = TIM2_CCMR2;
  /* Disable the Channel 2: Reset the CCE Bit */
  TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC2E));
  /* Reset the Output Compare Bits */
  tmpccmr2 &= (uint8_t)(~(TIM_CCMR_OCM));
  /* Set the Output Compare Mode */
  tmpccmr2 |= (uint8_t)0x60; //TIM2_OCMode_PWM1;
  TIM2_CCMR2 = tmpccmr2;

  /* Set the Output State */
  TIM2_CCER1 |= 1<<TIM_CCER1_CC2E;

  /* Set the Output Polarity */
  // TIM2_OCPolarity_High
  TIM2_CCER1 &= (uint8_t)(~(1<<TIM_CCER1_CC2P));

  /* Set the Output Idle state */
  TIM2_OISR |= 1<<TIM_OISR_OIS2;

  /* Set the Pulse value */
  TIM2_CCR2H = (uint8_t)(TIM2_Pulse >> 8);
  TIM2_CCR2L = (uint8_t)(TIM2_Pulse);
}


void tim2_ETRConfig(void)
{
  /* Configure the ETR Clock source */
  //TIM2_ETRConfig(TIM2_ExtTRGPSC_DIV4 : 0x20, TIM2_ExtTRGPolarity_NonInverted : 0x00, 0);
  TIM2_ETR |= (uint8_t)((uint8_t)((uint8_t)0x20 | (uint8_t)0x00) // TIM2_ExtTRGPSC_DIV4, TIM2_ExtTRGPolarity_NonInverted
                         | (uint8_t)0);  // ExtTRGFilter

  /* Enable the External clock mode2 */
  TIM2_ETR |= (1<<TIM_ETR_ECE);
}


void tim2_BaseInit(uint16_t period)
{
 /* Set the Autoreload value */
  TIM2_ARRH = (uint8_t)(period >> 8) ;
  TIM2_ARRL = (uint8_t)(period);

  /* Set the Prescaler value */
  TIM2_PSCR = (uint8_t)(0x00); // TIM2_Prescaler_1

  /* Select the Counter Mode */
  TIM2_CR1 &= (uint8_t)((uint8_t)(~(TIM_CR1_CMS))) & ((uint8_t)(~(TIM_CR1_DIR)));
  TIM2_CR1 |= (uint8_t)(0x00); // TIM2_CounterMode_Up

  /* Generate an update event to reload the Prescaler value immediately */
  TIM2_EGR = 0x01; //TIM2_EventSource_Update
}