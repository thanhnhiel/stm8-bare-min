
#ifndef RTC_H
#define RTC_H


#ifndef STM8L_H
#error "STM8L_H must be included before GPIO.H"
#endif


// RTC time in human readable format
typedef struct {
    uint8_t Hours;   // RTC hours
    uint8_t Minutes; // RTC minutes
    uint8_t Seconds; // RTC seconds
} rtc_TimeDef;

// RTC date in human readable format
typedef struct {
    uint8_t Year;  // RTC year (0..99)
    uint8_t Month; // RTC month (1..12)
    uint8_t Day;   // RTC date (1..31)
    uint8_t DOW;   // RTC day of week (1..7)
} rtc_DateDef;


void rtc_unlock(void);
void rtc_lock(void);

void rtc_TurnOnLSE(uint8_t enable);

// 0x0040: RTC_IT_WUT
// 0x0010: RTC_IT_ALRA
void rtc_ItConfig(uint8_t rtcIT);

void rtc_it_config(void);
void rtc_wakeup_clock_config(uint16_t devnum);



void rtc_setWakeUpCounter(uint16_t count);
void rtc_wakeUpCmd(uint8_t enable);
void rtc_AlarmCmd(uint8_t enable);
void rtc_SetAlarmTime(rtc_TimeDef *time, uint8_t WeekDay);

uint8_t rtc_getIT_WUT(void);
void rtc_clearIT_WUT(void);

uint8_t rtc_getIT_ALRA(void);
void rtc_clearIT_ALRA(void);

uint8_t rtc_Init(void);
void rtc_GetDate(rtc_DateDef *date);
void rtc_SetDate(rtc_DateDef *date);
void rtc_GetTime(rtc_TimeDef *time);
void rtc_SetTime(rtc_TimeDef *time);
uint8_t rtc_WaitForSynchro(void);

#endif
