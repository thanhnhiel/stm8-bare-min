#ifndef TIM2_H
#define TIM2_H

#ifndef F_CPU
#warning "F_CPU not defined, using 2MHz by default"
#define F_CPU 2000000UL
#endif

#include <stdint.h>

void SetOnPWM1(uint16_t t);
void SetOffPWM1(void);
void SetOffPWM2(void);
void SetOnPWM2(uint16_t t);

void tmr2_OC1Init(uint16_t TIM2_Pulse);
void tmr2_OC2Init(uint16_t TIM2_Pulse);
void tim2_ETRConfig(void);
void tim2_BaseInit(uint16_t period);

#endif /* DELAY_H */
