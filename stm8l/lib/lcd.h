#ifndef LCD_H
#define LCD_H

#ifndef F_CPU
#warning "F_CPU not defined, using 2MHz by default"
#define F_CPU 2000000UL
#endif

#include <stdint.h>

#define LCD_ADDR                    0x3C //(0x78)

// registers
#define SSD1306_CONTROL_REG 0x00  // Co = 0, DC = 0
#define SSD1306_DATA_REG 0x40


#define SSD1306_MULTIPLEX_RATIO 0xA8

#define SSD1306_MEMORYMODE          0x20 ///< See datasheet
#define SSD1306_COLUMNADDR          0x21 ///< See datasheet
#define SSD1306_PAGEADDR            0x22 ///< See datasheet
#define SSD1306_SETCONTRAST         0x81 ///< See datasheet
#define SSD1306_CHARGEPUMP          0x8D ///< See datasheet
#define SSD1306_SEGREMAP            0xA0 ///< See datasheet
#define SSD1306_DISPLAYALLON_RESUME 0xA4 ///< See datasheet
#define SSD1306_DISPLAYALLON        0xA5 ///< Not currently used
#define SSD1306_NORMALDISPLAY       0xA6 ///< See datasheet
#define SSD1306_INVERTDISPLAY       0xA7 ///< See datasheet
#define SSD1306_SETMULTIPLEX        0xA8 ///< See datasheet
#define SSD1306_DISPLAYOFF          0xAE ///< See datasheet
#define SSD1306_DISPLAYON           0xAF ///< See datasheet
#define SSD1306_COMSCANINC          0xC0 ///< Not currently used
#define SSD1306_COMSCANDEC          0xC8 ///< See datasheet
#define SSD1306_SETDISPLAYOFFSET    0xD3 ///< See datasheet
#define SSD1306_SETDISPLAYCLOCKDIV  0xD5 ///< See datasheet
#define SSD1306_SETPRECHARGE        0xD9 ///< See datasheet
#define SSD1306_SETCOMPINS          0xDA ///< See datasheet
#define SSD1306_SETVCOMDETECT       0xDB ///< See datasheet

#define SSD1306_SETLOWCOLUMN        0x00 ///< Not currently used
#define SSD1306_SETHIGHCOLUMN       0x10 ///< Not currently used
#define SSD1306_SETSTARTLINE        0x40 ///< See datasheet


#define SSD1306_RIGHT_HORIZONTAL_SCROLL              0x26 ///< Init rt scroll
#define SSD1306_LEFT_HORIZONTAL_SCROLL               0x27 ///< Init left scroll
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29 ///< Init diag scroll
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL  0x2A ///< Init diag scroll
#define SSD1306_DEACTIVATE_SCROLL                    0x2E ///< Stop scroll
#define SSD1306_ACTIVATE_SCROLL                      0x2F ///< Start scroll
#define SSD1306_SET_VERTICAL_SCROLL_AREA             0xA3 ///< Set scroll range

#define I2C_SPEED      100000 /*!< I2C Speed */
//#define I2C_ADDR 0x3D //(3C)

#define I2C_ADDR 0x78 //(3C)
//#define I2C_ADDR 0x7A //(3D)

#define WIDTH 128
#define HEIGHT 64

//#define SSD1306_EXTERNALVCC         0x00 ///< External display voltage source
#define SSD1306_SWITCHCAPVCC        0x01 ///< Gen. display voltage from 3.3V



void lcd_write(char *buf, uint16_t num);
void lcd_init(void);


void lcd_init(void);
//void lcd_lowlevel_init(void);
//void lcd_set(void);
//void lcd_clear(void);
//void lcd_setval(uint8_t val);

void updateDisplay(uint8_t val);
void displayOff(void);
void displayOn(void);
void clearDisplay(void);
void sendCommand(uint8_t command);
void sendData(uint8_t data);
void draw(uint8_t x, uint8_t y, uint8_t ascii);
void draw_line(uint8_t x ,uint8_t y, uint8_t *ascii_str);

#endif /* DELAY_H */
