#include "i2c.h"
#include "stm8l.h"

// 5 seconds
//#define I2C_TIMEOUT (uint32_t)((uint32_t)(F_CPU / 18 / 1000UL) * (uint32_t)5)
#define I2C_TIMEOUT 100

void i2c_init() {
    I2C1_FREQR = (1 << I2C1_FREQR_FREQ1);
    I2C1_CCRL = 0x0A; // 100kHz
    I2C1_OARH = (1 << I2C1_OARH_ADDMODE); // 7-bit addressing
    /* I2C_Cmd(I2C1, Enable); */
    I2C1_CR1 = (1 << I2C1_CR1_PE);
}

void i2c_start() {
    uint32_t wait = I2C_TIMEOUT;

    I2C1_CR2 |= (1 << I2C1_CR2_START);
    while (!(I2C1_SR1 & (1 << I2C1_SR1_SB)) && --wait);
}

void i2c_stop() {
    uint32_t wait = I2C_TIMEOUT;
    
    I2C1_CR2 |= (1 << I2C1_CR2_STOP);
    while ((I2C1_SR3 & (1 << I2C1_SR3_MSL)) && --wait);
}

uint8_t i2c_write(uint8_t data) {
    uint32_t wait = I2C_TIMEOUT;
    
    I2C1_DR = data;
    while ((!(I2C1_SR1 & (1 << I2C1_SR1_TXE))) && --wait);
    if (wait==0) return 0;
    return 1;
}

void i2c_write_addr(uint8_t addr) {
    uint32_t wait = I2C_TIMEOUT;
    
    I2C1_DR = addr;
    while ((!(I2C1_SR1 & (1 << I2C1_SR1_ADDR))) && --wait);
    (void) I2C1_SR3; // check BUS_BUSY
    I2C1_CR2 |= (1 << I2C1_CR2_ACK);
}

uint8_t i2c_read() {
    uint32_t wait = I2C_TIMEOUT;
    
    I2C1_CR2 &= ~(1 << I2C1_CR2_ACK);
    i2c_stop();
    while ((!(I2C1_SR1 & (1 << I2C1_SR1_RXNE))) && --wait);
    return I2C1_DR;
}

void i2c_read_arr(uint8_t *buf, int len) {
    uint32_t wait = I2C_TIMEOUT;
    
    while (len-- > 1 && wait) 
    {
        I2C1_CR2 |= (1 << I2C1_CR2_ACK);
        wait = I2C_TIMEOUT;
        while ((!(I2C1_SR1 & (1 << I2C1_SR1_RXNE))) && --wait);
        *(buf++) = I2C1_DR;
    }
    *buf = i2c_read();
}
