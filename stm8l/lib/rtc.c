
#include <stdint.h>
#include "stm8l.h"
#include "rtc.h"

void rtc_unlock(void)
{
  /* Disable the write protection for RTC registers */
  RTC_WPR = 0xCA;
  RTC_WPR = 0x53;
}

void rtc_lock(void)
{
  /* Enable the write protection for RTC registers */
  RTC_WPR = 0xFF;  
}

void rtc_TurnOnLSE(uint8_t enable)
{
  /* Reset LSEON and LSEBYP bits before configuring the LSE ------------------*/
  /* Reset LSEON bit */
  CLK_ECKCR &= (uint8_t)~(1<<CLK_ECKCR_LSEON);

  /* Reset LSEBYP bit */
  CLK_ECKCR &= (uint8_t)~(1<<CLK_ECKCR_LSEBYP);

  /* Configure LSE */
  if (enable)
  {
    CLK_ECKCR |= (uint8_t)(1<<CLK_ECKCR_LSEON);
  }

}

void rtc_it_config(void)
{
  rtc_unlock();
  //RTC_ITConfig(RTC_IT_WUT, ENABLE);
  RTC_CR2 |= (uint8_t)(0x0040);
  RTC_TCR1 |= (uint8_t)((uint16_t)0x0040 & RTC_TCR1_TAMPIE);
  rtc_lock();
}

// 0x0040: RTC_IT_WUT
// 0x0010: RTC_IT_ALRA
void rtc_ItConfig(uint8_t rtcIT)
{
  rtc_unlock();
  //RTC_ITConfig(RTC_IT_WUT, ENABLE);
  RTC_CR2 |= (uint8_t)(rtcIT);
  RTC_TCR1 |= (uint8_t)((uint16_t)rtcIT & RTC_TCR1_TAMPIE);
  rtc_lock();
}

void rtc_wakeup_clock_config(uint16_t devnum)
{
  rtc_unlock();
  //RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
  RTC_CR2 &= (uint8_t)~(1<<RTC_CR2_WUTE);
  /* Clear the Wakeup Timer clock source bits in CR1 register */
  RTC_CR1 &= (uint8_t)~RTC_CR1_WUCKSEL;
  /* Configure the clock source */
  //RTC_CR1 |= (uint8_t)devnum; // 0x04 - RTC_WakeUpClock_CK_SPRE_16bits
  RTC_CR1 |= (uint8_t)devnum; // 0 - RTC_WakeUpClock_RTCCLK_Div16
  rtc_lock();  
}

void rtc_setWakeUpCounter(uint16_t count)
{
  rtc_unlock();
  //RTC_SetWakeUpCounter(1);
  RTC_WUTRH = (uint8_t)(count >> 8);
  RTC_WUTRL = (uint8_t)(count);
  rtc_lock();  
}

void rtc_AlarmCmd(uint8_t enable)
{
    rtc_unlock();
    if (enable==1)
    {
        RTC_CR2 |= (uint8_t)(1<<RTC_CR2_ALRAE);
    }
    else
    {
        uint16_t wutwfcount = 0;

        RTC_CR2 &= (uint8_t)~(1<<RTC_CR2_ALRAE);
        
        /* Wait until WUTWF flag is set */
        while (((RTC_ISR1 & (1<<RTC_ISR1_ALRAWF)) == 0) && ( wutwfcount != 0xFFFF))
        {
            wutwfcount++;
        }
    }
    rtc_lock();
}

void rtc_wakeUpCmd(uint8_t enable)
{
    rtc_unlock();
    if (enable==1)
    {
        RTC_CR2 |= (uint8_t)(1<<RTC_CR2_WUTE);
    }
    else
    {
        uint16_t wutwfcount = 0;

        RTC_CR2 &= (uint8_t)~(1<<RTC_CR2_WUTE);
        
        /* Wait until WUTWF flag is set */
        while (((RTC_ISR1 & (1<<RTC_ISR1_WUTWF)) == 0) && ( wutwfcount != 0xFFFF))
        {
            wutwfcount++;
        }
    }
    rtc_lock();
}
//=======================================

uint8_t rtc_getIT_ALRA(void)
{
    return ((RTC_ISR2 & (1<<RTC_ISR2_ALRAF)) !=0);
}

void rtc_clearIT_ALRA(void)
{
    RTC_ISR2 = (uint8_t)~(uint8_t)(1<<RTC_ISR2_ALRAF);
}

uint8_t rtc_getIT_WUT(void)
{
    return ((RTC_ISR2 & (1<<RTC_ISR2_WUTF)) !=0);
}

void rtc_clearIT_WUT(void)
{
    RTC_ISR2 = (uint8_t)~(uint8_t)(1<<RTC_ISR2_WUTF);
}

//=======================================
static uint8_t Bcd2ToByte(uint8_t Value)
{
  uint8_t tmp = 0;

  tmp = (uint8_t)((uint8_t)((uint8_t)(Value & (uint8_t)0xF0) >> 4) * (uint8_t)10);

  return (uint8_t)(tmp + (Value & (uint8_t)0x0F));
}

// Get the RTC current time
// Output: rtc_TimeDef structure with current time
void rtc_GetTime(rtc_TimeDef *time) 
{
    uint8_t t1,t2,t3;

    t1 = RTC_TR1;
    t2 = RTC_TR2;
    t3 = RTC_TR3; // Hours
    /* Read DR3 register to unfreeze calender registers */
    (void) (RTC_DR3) ;    
    
    t3 = (uint8_t)(t3 & (uint8_t)~(0x40 )); // RTC_TR3_PM
    time->Hours = (uint8_t)Bcd2ToByte(t3);
    time->Minutes = (uint8_t)Bcd2ToByte(t2);
    time->Seconds = (uint8_t)Bcd2ToByte(t1);    

    
    //time->Seconds = ((t1 >> 4) * 10) + (t1 & 0x0F);
    //time->Minutes = ((t2 >> 4) * 10) + (t2 & 0x0F);
    //time->Hours   = (((t3 & 0x30) >> 4) * 10) + (t3 & 0x0F);
}

// Get the RTC current date
// Output: rtc_DateDef structure with current date
void rtc_GetDate(rtc_DateDef *date)
{
    uint8_t d1,d2,d3;

    d1 = RTC_DR1;
    d2 = RTC_DR2;
    d3 = RTC_DR3;

    date->Day   = ((d1 >> 4) * 10) + (d1 & 0x0F);
    date->DOW   = d2 >> 5;
    date->Month = (((d2 & 0x1F) >> 4) * 10) + (d2 & 0x0F);
    date->Year  = ((d3 >> 4) * 10) + (d3 & 0x0F);
}

static uint8_t rtc_EnterInitMode(void)
{
  uint16_t initfcount = 0;

  /* Check if the Initialization mode is set */
  if ((RTC_ISR1 & (1<<RTC_ISR1_INITF))==0)
  {
    /* Set the Initialization mode */
    RTC_ISR1 |= (1<<RTC_ISR1_INIT);

    /* Wait until INITF flag is set */
    while (((RTC_ISR1 & (1<<RTC_ISR1_INITF))==0) && ( initfcount != 0xFFFF))
    {
      initfcount++;
    }
  }

  if ((RTC_ISR1 & (1<<RTC_ISR1_INITF))==0)
  {
    return 0;
  }
  else
  {
    return 1; // OK
  }
}

uint8_t rtc_Init(void) 
{
  rtc_unlock(); // Disable write protection of RTC registers

  if (rtc_EnterInitMode() == 0)
  {
    //status = ERROR;
    rtc_lock(); // Enable write protection of RTC registers  
    return 0;    
  }
  else
  {
    /* Clear the bits to be configured first */
    RTC_CR1 &= ((uint8_t)~( 0x40 )); // RTC_CR1_FMT
    /* Set RTC_CR1 register */
    //RTC_CR1 |=  ((uint8_t)(0)); // RTC_HourFormat_24
    /* Set Prescalers registers */
    RTC_SPRERH = (uint8_t)(0x00FF >> 8);
    RTC_SPRERL = (uint8_t)(0x00FF);
    RTC_APRER =  (uint8_t)(0x7F);
    
    /* Exit Initialization mode */
    RTC_ISR1 &= ~(1<<RTC_ISR1_INIT); // Exit initialization mode
    
    rtc_lock(); // Enable write protection of RTC registers 
    return 1;
  }
}

static uint8_t ByteToBcd2(uint8_t Value)
{
  uint8_t bcdhigh = 0;

  while (Value >= 10)
  {
    bcdhigh++;
    Value -= 10;
  }

  return  (uint8_t)((uint8_t)(bcdhigh << 4) | Value);
}

uint8_t rtc_WaitForSynchro(void)
{
    uint32_t wait = 0xFFFFFF; // RTC_LSE_INIT_TIMEOUT;
    uint8_t status = 0;
    
    rtc_unlock(); // Disable write protection of RTC registers
    
    /* Clear RSF flag by writing 0 in RSF bit  */
    RTC_ISR1 &= (uint8_t)~(1<<RTC_ISR1_INIT | 1<<RTC_ISR1_RSF); 
    /* Wait the registers to be synchronised */
    wait = 0xFFFFFF;
    while ((RTC_ISR1 & (1<<RTC_ISR1_RSF))==0 && --wait);
        
    /* Check if RSF flag occurs*/
    if ((RTC_ISR1 & (1<<RTC_ISR1_RSF)) != 0)
    {
      status = 1;
    }
    else
    {
      status = 0;
    }        
    rtc_lock(); // Enable write protection of RTC registers    
    
    return status; 
}

void rtc_SetAlarmTime(rtc_TimeDef *time, uint8_t WeekDay)
{
  uint8_t AlarmMask = 0x01;
#define  SEL_WEEKDAY 0x00

#if 0    
    RTC_AlarmDateWeekDaySel_Date     = ((uint8_t)0x00), /*!< Date/WeekDay selection is Date */
    RTC_AlarmDateWeekDaySel_WeekDay  = ((uint8_t)0x40)  /*!< Date/WeekDay selection is WeekDay */    
   /* 
    * bit 6 : off for select RTC_AlarmDateDate
    * bit 6 : on for select RTC_AlarmDateWeekDay
    * bit 7: For Enable AlarmDate 
    */    
#endif   


    rtc_unlock(); // Disable write protection of RTC registers
    
    RTC_ALRMAR1 = (uint8_t)(ByteToBcd2(time->Seconds) | (uint8_t)((AlarmMask ) & 0x80));
    RTC_ALRMAR2 = (uint8_t)(ByteToBcd2(time->Minutes) | (uint8_t)((AlarmMask << 1) & 0x80));
    RTC_ALRMAR3 = (uint8_t)(ByteToBcd2(time->Hours) | (uint8_t)((AlarmMask << 2) & 0x80));
    /* RTC_AlarmDateWeekDay */
   
    RTC_ALRMAR4 = (uint8_t)(ByteToBcd2(WeekDay) | (uint8_t)((AlarmMask << 3) & 0x80) | (SEL_WEEKDAY));

    rtc_lock(); // Enable write protection of RTC registers 
    
    if ((RTC_CR1 & (1<<RTC_CR1_BYPSHAD)) == 0)
    {
        rtc_WaitForSynchro();
    }
}

void rtc_SetTime(rtc_TimeDef *time) 
{
    uint32_t wait = 0xFFFFFF; // RTC_LSE_INIT_TIMEOUT;
    
    rtc_unlock(); // Disable write protection of RTC registers
    
    RTC_ISR1 |= (1<<RTC_ISR1_INIT); // Enter initialization mode
    // Poll INITF flag until it is set in RTC_ISR1.
    // It takes around 2 RTCCLK clock cycles according to datasheet
    wait = 0xFFFFFF;
    while ((RTC_ISR1 & (1<<RTC_ISR1_INITF))==0 && --wait);
    if (!wait) { rtc_lock(); return; }
    
    RTC_TR1 = (uint8_t)(ByteToBcd2(time->Seconds));
    RTC_TR2 = (uint8_t)(ByteToBcd2(time->Minutes)) ;
    RTC_TR3 = (uint8_t)(ByteToBcd2(time->Hours));
    /* Read DR3 register to unfreeze calender registers */
    (void)(RTC_DR3);
    
    RTC_ISR1 &= ~(1<<RTC_ISR1_INIT); // Exit initialization mode
    rtc_lock(); // Enable write protection of RTC registers 
       
    if ((RTC_CR1 & (1<<RTC_CR1_BYPSHAD)) == 0)
    {
        rtc_WaitForSynchro();
    }
}

void rtc_SetDate(rtc_DateDef *date)
{
    uint32_t wait = 0xFFFFFF; // RTC_LSE_INIT_TIMEOUT;
    
    rtc_unlock(); // Disable write protection of RTC registers
    
    RTC_ISR1 |= (1<<RTC_ISR1_INIT); // Enter initialization mode
    // Poll INITF flag until it is set in RTC_ISR1.
    // It takes around 2 RTCCLK clock cycles according to datasheet
    wait = 0xFFFFFF;
    while ((RTC_ISR1 & (1<<RTC_ISR1_INITF))==0 && --wait);
    if (!wait) { rtc_lock(); return; }
    
    (void)(RTC_TR1);
    RTC_DR1 = (uint8_t)(ByteToBcd2 ((uint8_t)date->Day));
    RTC_DR2 = (uint8_t)((ByteToBcd2((uint8_t)date->Month)) | (uint8_t)((date->DOW) << 5));
    RTC_DR3 = (uint8_t)(ByteToBcd2((uint8_t)date->Year));


    RTC_ISR1 &= ~(1<<RTC_ISR1_INIT); // Exit initialization mode
    rtc_lock(); // Enable write protection of RTC registers    
    
    
    if ((RTC_CR1 & (1<<RTC_CR1_BYPSHAD)) == 0)
    {
        rtc_WaitForSynchro();
    }
}

