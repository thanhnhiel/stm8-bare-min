#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <stm8l.h>
#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "rtc.h"
#include "tim2.h"
#include "lcd.h"
#include "interrupt.h"


//#define LED_GPIO_PORT  PORTC
//#define LED_GPIO_PINS  PIN4

#define LCD_PWR_PORT  PORTB
#define LCD_PWR_PINS  PIN7

#define LED_GPIO_PORT  PORTB
#define LED_GPIO_PINS  PIN1

void lcd_lowlevel_init(void);
void initGPIO(void);
void initClock(void);
void initCalendar(void);
void TIM2_Config(void);
void WaitPWM(uint8_t count);

volatile uint8_t wuf = 0;
volatile uint8_t alarmf = 0;
rtc_TimeDef time;
rtc_DateDef date;

/*
 * Redirect stdout to UART
 */
int putchar(int c) {
    uart_write(c);
    return 0;
}

/*
 * Redirect stdin to UART
 */
int getchar() {
    return uart_read();
}


volatile uint8_t PulseCount;

#if 0
void tim2_upd_isr() __interrupt(TIM2_UPD_ISR) 
{
  //TIM2_ClearITPendingBit(TIM2_IT_Update);
  TIM2_SR1 = (uint8_t)(~(uint8_t)(1<<0));

  if (PulseCount)
  {
    PulseCount--;
    if (PulseCount==0)
    {
      //TIM2_CCxCmd(TIM2_Channel_1, DISABLE);
      TIM2_CCER1 &= ~(1<<TIM_CCER1_CC1E);
    }
  }
}
#endif

void tim2_cc_isr() __interrupt(TIM2_CC_ISR) 
{
  //TIM2_ClearITPendingBit(TIM2_IT_CC1);
  TIM2_SR1 = (uint8_t)(~(uint8_t)(1<<1));

  //PinLow(LED_GPIO_PORT, LED_GPIO_PINS); // LED On

  if (PulseCount)
  {
    PulseCount--;
    if (PulseCount==0)
    {
      //TIM2_CCxCmd(TIM2_Channel_1, DISABLE);
      TIM2_CCER1 &= ~(1<<TIM_CCER1_CC1E);
    }
  }
}

void WaitPWM(uint8_t count)
{
    PulseCount = count;
    delay_ms(10);
    while(PulseCount) ;
}

/* rtc_isr functions */
void rtc_isr() __interrupt(RTC_ISR) 
{
    if (rtc_getIT_WUT())
    {
        rtc_clearIT_WUT();
        //RTC_ISR2 = (uint8_t)~(uint8_t)(1<<RTC_ISR2_WUTF);
        //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
        wuf = 1;
    }
    
    if (rtc_getIT_ALRA())
    {
      rtc_clearIT_ALRA();
      alarmf = 1;
    }
}

#define CountOn   (4+1) // 5mS
#define CountOff  (4*2+1) // 10mS
#define PulsNum   20 // 20, 50

void main(void) 
{
  uint8_t tick=0, sec;
  
  initClock();
  TIM2_Config();

  initGPIO();
  lcd_lowlevel_init();
  /* Wait LCD On */
  delay_ms(40); 

  lcd_init();
  draw_line(0,1,"Hello");
  
  PinHigh(LED_GPIO_PORT, LED_GPIO_PINS); // LED Off
  
  enable_interrupts();
  while(1)
  {
    delay_ms(2000);
    draw_line(0,1,"1");
    
    PulseCount = 10;
    
    SetOnPWM1(4*4); // 0.5mS 
    SetOnPWM2(4*4); // 0.5mS 
    draw_line(0,1,"2");
    while(PulseCount); // 10
    draw_line(0,1,"3");
    SetOffPWM1();
    SetOffPWM2(); 
    delay_ms(1000);
    
    //PinHigh(LED_GPIO_PORT, LED_GPIO_PINS);
    draw_line(0,1,"4");
    PulseCount = 10;
    
    SetOnPWM1(4); // 0.5mS 
    SetOnPWM2(4); // 0.5mS 
    
    while(PulseCount); // 10
    draw_line(0,1,"5");
    SetOffPWM1();
    SetOffPWM2();
    draw_line(0,1,"6");
    
    //PinLow(LED_GPIO_PORT, LED_GPIO_PINS);

  }
  
  initCalendar();
  
  //SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  SYSCFG_RMPCR1 &= ~((1<<4) | (1<<5));
  SYSCFG_RMPCR1 |= (1<<4);  
  uart_init();
  

  rtc_wakeup_clock_config(0x04); // 0x04: RTC_WakeUpClock_CK_SPRE_16bits
  rtc_it_config();
 
  time.Hours = 0;
  time.Minutes = 00;
  time.Seconds = 10;
  rtc_SetAlarmTime(&time, 2); // 2 : Tuesday

  rtc_ItConfig(0x10); // RTC_IT_ALRA : 0x10
  rtc_AlarmCmd(1);
  
  uart_write('\r');
  uart_write('\n');
  uart_write('-');
  uart_write('-');
  
  rtc_setWakeUpCounter(6); // 2 seconds
  rtc_wakeUpCmd(1);  

  while(1)  
  {
    /* CPU in Active Halt mode */
    __asm__("halt");
    
    if (alarmf)
    {
      alarmf = 0;      
      rtc_GetTime(&time);
      
      uart_write('\r');
      uart_write('\n');
      uart_write('A');
      uart_write('L');
      uart_write('A');
      uart_write('R');
      uart_write('M');
      uart_write(' ');
      
      sec = time.Hours;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');

      sec = time.Minutes;
      uart_write(':');
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');      
      
      sec = time.Seconds;
      uart_write(':');
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');       
    }    
    else if (wuf)
    {
      wuf = 0;
      PinLow(LED_GPIO_PORT, LED_GPIO_PINS);
      rtc_GetTime(&time);
      
      uart_write('\r');
      uart_write('\n');
      
      sec = time.Hours;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');
      
      uart_write(':');
      sec = time.Minutes;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0'); 
      
      uart_write(':');
      sec = time.Seconds;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');

      PinHigh(LED_GPIO_PORT, LED_GPIO_PINS);  
      //printf("Test, %002d:%002d:%002d\r\n", time.Hours, time.Minutes, time.Seconds);
      //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
    }

  }
  

#if 0
    
  //enable_interrupts();
      
  while (1) 
  {
    /* CPU in Active Halt mode */
    __asm__("halt");

    if (wuf)
    {
      wuf = 0;
      tick++;
      
      if (tick==1)
      {
         PinLow(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==2)
      {
         PinHigh(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==5)
      {
        tick = 0;
        rtc_GetTime(&time);
        rtc_GetDate(&date);
      }
      
      /* Toggle LEDs LD1..LD4 */
      //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
    }
  }
#endif  
}

/* ==================================================================== */
void lcd_lowlevel_init(void)
{
  /*!< I2C1 Periph clock enable */
  //CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
  CLK_PCKENR1 |= (uint8_t)((uint8_t)1 << ((uint8_t)0x03));


  /*!< Configure sEE_I2C pins: SCL */
  //GPIO_Init(GPIOC, GPIO_Pin_1, GPIO_Mode_In_PU_No_IT:0x40);
  PC_DDR &= (uint8_t)(~(1<<PIN1));
  PC_CR1 |= 1<<PIN1;
  PC_CR2 &= (uint8_t)(~(1<<PIN1));

  /*!< Configure sEE_I2C pins: SDA */
  //GPIO_Init(GPIOC, GPIO_Pin_0, GPIO_Mode_In_PU_No_IT);
  PC_DDR &= (uint8_t)(~(1<<PIN0));
  PC_CR1 |= 1<<PIN0;
  PC_CR2 &= (uint8_t)(~(1<<PIN0));

  //GPIO_Init(LCD_PWR_PORT, LCD_PWR_PIN, GPIO_Mode_Out_PP_Low_Fast); 
  /* Set Output mode */
  PinOutput(LCD_PWR_PORT, LCD_PWR_PINS);
  /* Pull-Up/Float (Input) or Push-Pull/Open-Drain (Output) modes selection */
  PinOutputPullUp(LCD_PWR_PORT, LCD_PWR_PINS);
  /* Interrupt (Input) or Slope (Output) modes selection */
  PinOutputSpeed10MHZ(LCD_PWR_PORT, LCD_PWR_PINS);
  PinLow(LCD_PWR_PORT, LCD_PWR_PINS); // Turn On LCD  
}

void initGPIO(void)
{
  /* 4.5uA with regulator(2uA), 2.5uA only stm8 */
  /* Input Pull Up all PORTs */
  //PA_DDR = 0;
  //PB_DDR = 0;
 //PC_DDR = 0;
  //PD_DDR = 0;
  /* Need to - 80uA */
  PA_CR1 = 0xFF;
  PB_CR1 = 0xFF;
  PC_CR1 = 0xFF;
  PD_CR1 = 0xFF;
  PinOutputFloat(PORTD, PIN0); // No Need
  //PinOutputFloat(PORTC, PIN5); // No Need
  //PinOutputFloat(PORTC, PIN6); // No Need
  // PinOutputFloat(PORTA, PIN2); => Increate current TXD
  //PinOutputFloat(PORTA, PIN0); // SWIM 2uA

  /* Config LED Port */
  PinOutput(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputPullUp(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputSpeed10MHZ(LED_GPIO_PORT, LED_GPIO_PINS);
  /* Turn off Led */
  PinLow(LED_GPIO_PORT, LED_GPIO_PINS);

  /* PWM1 - TIM2_CH1 - PB0 - GPIO_Mode_Out_PP_Low_Fast*/
  PinOutput(PORTB, PIN0);
  PinOutputPullUp(PORTB, PIN0);
  PinOutputSpeed10MHZ(PORTB, PIN0);
  /* PWM2 - TIM2_CH2 - PB2 - GPIO_Mode_Out_PP_Low_Fast */
  PinOutput(PORTB, PIN2);
  PinOutputPullUp(PORTB, PIN2);
  PinOutputSpeed10MHZ(PORTB, PIN2);
}

void initClock(void)
{
 
  /* CLK configuration */
  /* CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);  */
  //CLK_CKDIVR = (uint8_t)(0x00); // CLK_SYSCLKDiv_1 = 0x00
  CLK_CKDIVR = (uint8_t)(0x03); // CLK_SYSCLKDiv_8
  
  rtc_TurnOnLSE(1);
  /* Wait for LSERDY flag to be reset */
  while ((CLK_ECKCR & (1<<CLK_ECKCR_LSERDY)) == 0);

  /* Configures RTC clock */
  CLK_CRTCR = (uint8_t)((uint8_t)CLK_CRTCR_Source_LSE | (uint8_t)CLK_CLKDiv_1); // 
  /* Enable RTC */
  CLK_PCKENR2 |= (uint8_t)(1 << CLK_PCKENR2_RTC); 
}

void initCalendar(void)
{
  rtc_TimeDef time;
  rtc_DateDef date;
  
  //Calendar_Init()
  rtc_Init();
  
  date.DOW = 1;
  date.Day = 29;
  date.Month = 7;
  date.Year = 19;
  rtc_SetDate(&date);

  time.Hours = 23;
  time.Minutes = 59;
  time.Seconds = 50;
  rtc_SetTime(&time);
}


/**
  * @brief  Configure TIM1 peripheral 
  * @param  None
  * @retval None
  */
#define TIM2_PERIOD  (uint8_t) 159 // 8 -> 1mS
#define TIM2_PULSE   (uint8_t) 4   // 0.5mS

void TIM2_Config(void)
{
    /* Enable TIM2 clock */
  //CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  CLK_PCKENR1 |= (uint8_t)((uint8_t)1 << CLK_PCKENR1_TIM2_EN );

  /* Remap TIM2 ETR to LSE: TIM2 external trigger becomes controlled by LSE clock */
  //SYSCFG_REMAPPinConfig(REMAP_Pin_TIM2TRIGLSE, ENABLE);
  SYSCFG_RMPCR2 |= (uint8_t) (1<<SYSCFG_RMPCR2_TIM2TRIGLSE);

  /* TIM2 configuration:
     - TIM2 ETR is mapped to LSE
     - TIM2 counter is clocked by LSE div 4
      so the TIM2 counter clock used is LSE / 4 = 32.768 / 4 = 8.192 KHz
    TIM2 Channel1 output frequency = TIM2CLK / (TIM2 Prescaler * (TIM2_PERIOD + 1))
                                   = 8192 / (1 * 8) = 1024 Hz */
  /* Time Base configuration */
  //TIM2_TimeBaseInit(TIM2_Prescaler_1, TIM2_CounterMode_Up, TIM2_PERIOD);
  tim2_BaseInit(TIM2_PERIOD);
  
  //TIM2_ETRClockMode2Config(TIM2_ExtTRGPSC_DIV4, TIM2_ExtTRGPolarity_NonInverted, 0);
  tim2_ETRConfig();

  /* Channel 1 configuration in PWM1 mode */
  /* TIM2 channel Duty cycle is 100 * TIM2_PULSE / (TIM2_PERIOD + 1) = 100 * 4/8 = 50 % */
  //TIM2_OC1Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, TIM2_PULSE, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  tmr2_OC1Init(TIM2_PULSE);
  //TIM2_OC2Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, TIM2_PULSE, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  tmr2_OC2Init(TIM2_PULSE);
  
  /* TIM2 Main Output Enable */
  //TIM2_CtrlPWMOutputs(ENABLE);
  TIM2_BKR |= (TIM_BKR_MOE) ;
  
  /* TIM2 counter enable */
  //TIM2_Cmd(ENABLE);
  TIM2_CR1 |= (TIM_CR1_CEN);
  
  //TIM2_ITConfig(TIM2_IT_CC1, ENABLE);
  TIM2_IER |= (uint8_t)(0x02); // TIM2_IT_CC1
}

