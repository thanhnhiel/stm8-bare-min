#include <stdint.h>
#include <stm8l.h>
#include <delay.h>

#define LED_PIN     4

void lowPower_Halt(void) ;
void InitRTC(void);

void rtc_isr() __interrupt(RTC_ISR) 
{
    //PC_ODR |= (1 << LED_PIN);
    RTC_ISR2 &= ~(1<<RTC_ISR2_WUTF); // clear the WUT Flag
    
        PC_ODR |= (1 << LED_PIN);
        delay_ms(20);
        PC_ODR &= ~(1 << LED_PIN);
}

void main() {
	PA_DDR = 0xFF;
	PB_DDR = 0xFF;
	PC_DDR = 0xFF;
	PD_DDR = 0xFF;
	PE_DDR = 0xFF;
    
    PC_DDR |= (1 << LED_PIN);
    PC_CR1 |= (1 << LED_PIN);
    PC_ODR &= ~(1 << LED_PIN);

    /* Enable TIM2 clock */
    CLK_PCKENR1 |= (uint8_t)((uint8_t)1 << 0);
    
    /* Remap TIM2 ETR to LSE: TIM2 external trigger becomes controlled by LSE clock */
    SYSCFG_RMPCR2 |= (uint8_t) 0x08; // REMAP_Pin_TIM2TRIGLSE

    /* Enable LSE clock */
    //CLK_LSEConfig(CLK_LSE_ON);
    CLK_ECKCR &= (uint8_t)~(1<<CLK_ECKCR_LSEON);
    /* Reset LSEBYP bit */
    CLK_ECKCR &= (uint8_t)~(1<<CLK_ECKCR_LSEBYP);
    /* Configure LSE */
    CLK_ECKCR |= (uint8_t)(1<<CLK_ECKCR_LSEON); // CLK_LSE;
    
    /* Wait for LSERDY flag to be reset */
    while ((CLK_ECKCR & (1 << CLK_ECKCR_LSERDY)) == 0) ;
    

    /* Configures RTC clock to CLK_RTCCLKDiv_1*/
    CLK_CRTCR = 0x10;
  
    //Enable RTC
    CLK_PCKENR2 |= (uint8_t)(1 << 2);
    // InitRTC();
#if 1 

  /* RTC_WakeUpClockConfig RTC_WakeUpClock_RTCCLK_Div8 */
  RTC_WPR = 0xCA;
  RTC_WPR = 0x53;
  /* Disable the Wakeup timer in RTC_CR2 register */
  RTC_CR2 &= (uint8_t)~(1<<RTC_CR2_WUTE);
  /* Clear the Wakeup Timer clock source bits in CR1 register */
  RTC_CR1 &= (uint8_t)~(0x07); // RTC_CR1_WUCKSEL
  /* Configure the clock source */
  RTC_CR1 |= (uint8_t)0x01;

  /* Enable the Interrupts */
  RTC_CR2 |= (uint8_t)((uint16_t)(1<<RTC_CR2_WUTE) & (uint16_t)0x00F0);
  RTC_TCR1 |= (uint8_t)((uint16_t)(1<<RTC_CR2_WUTE) & (1<<0));
  
  /* Enable the Wakeup Timer */
  RTC_CR2 |= (uint8_t)(1<<RTC_CR2_WUTE);

  /* Configure the Wakeup Timer counter */
  RTC_WUTRH = (uint8_t)(2047 >> 8);
  RTC_WUTRL = (uint8_t)(2047);

  /* Enable the write protection for RTC registers */
  RTC_WPR = 0xFF; 
#endif  

    //lowPower_Halt();
    
    //enable_interrupts();
    
    while (1) 
    {
        //PC_ODR ^= (1 << LED_PIN);
        //delay_ms(1000);
        __asm__("nop");
        __asm__("halt");
        
        RTC_ISR2 &= ~(1<<RTC_ISR2_WUTF);
        

        delay_ms(20);
    }
}

void lowPower_Halt(void) 
{ 
  // switch off main regulator during halt mode 
  //CLK_ICKR |= (1<<5);
  //CLK.ICKR.reg.REGAH = 1; // CLK.ICKR->bit5 = 1 

  // power down flash during halt mode 
  //FLASH_CR1 |= FLASH_CR1_AHALT;

  // enter HALT mode
  __asm__("halt");
} // lowPower_Halt 

void InitRTC(void)
{

	// Enable the RTC SYSCLK
	CLK_PCKENR2 |= 0x04;
	
	//Choose the RTC CLK source
	CLK_CRTCR |= 0xC4; // LSI / 64

	while (CLK_CRTCR & 0x01); // wait untile the busyflag is reseted
	
	// Unable write protection
	RTC_WPR = 0xCA;
	RTC_WPR = 0x53;
	
	// Set the Initialization mode 
	//RTC->ISR1 = 0x80;
	
	// disable the Timer
	RTC_CR2 &= 0xFB; // b11111011 - clear WUTE
	
	// Poll the WUTWF
	while (! RTC_ISR1 & 0x04 );
	//RTC_ISR1 |= 0x10;
	
	// Setup the Wakeup timer
	RTC_WUTRL = 0x0C;
	RTC_WUTRH = 0x00;
	
	// Setup the clock selection
	RTC_CR1 |= 0x03; // RTC/2
	
	// Enable the WUtimer interrupt
	RTC_CR2 |= 0x40;
	
	// Start the WU timer	
	RTC_CR2 |= 0x04;
}