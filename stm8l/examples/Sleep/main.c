#include <stdint.h>
#include <stm8l.h>
#include <delay.h>
#include "gpio.h"
#include "rtc.h"
#include "interrupt.h"


//#define LED_GPIO_PORT  PORTC
//#define LED_GPIO_PINS  PIN4

#define LED_GPIO_PORT  PORTB
#define LED_GPIO_PINS  PIN1


void initGPIO(void);
void initClock(void);


volatile uint8_t wuf = 0;

/* rtc_isr functions */
void rtc_isr() __interrupt(RTC_ISR) 
{
    if (rtc_getIT_WUT())
    {
        rtc_clearIT_WUT();
        //RTC_ISR2 = (uint8_t)~(uint8_t)(1<<RTC_ISR2_WUTF);
        //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
        wuf = 1;
    }
}


void main(void) 
{
  uint8_t tick=0;
  
  initClock();
  initGPIO();
  rtc_it_config();
  rtc_wakeup_clock_config(0x04); // 0x04: RTC_WakeUpClock_CK_SPRE_16bits
  
  delay_ms(1000);
  
  /* Set Wake Up count */
  rtc_setWakeUpCounter(1);
  /* Enable RTC_WakeUp */
  rtc_wakeUpCmd(1);
    
  //enable_interrupts();
      
  while (1) 
  {
    /* CPU in Active Halt mode */
    __asm__("halt");

    if (wuf)
    {
      wuf = 0;
      tick++;
      
      if (tick==1)
      {
         PinLow(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==2)
      {
         PinHigh(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==5)
      {
        tick = 0;  
      }
      
      /* Toggle LEDs LD1..LD4 */
      //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
    }
  }
}

/* ==================================================================== */
void initGPIO(void)
{
  /* Input Pull Up all PORTs */
  PA_DDR = 0;
  PB_DDR = 0;
  PC_DDR = 0;
  PD_DDR = 0;
  
  PA_CR1 = 0xFF;
  PB_CR1 = 0xFF;
  PC_CR1 = 0xFF;
  PD_CR1 = 0xFF;

  /* Config LED Port */
  PinOutput(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputPullUp(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputSpeed10MHZ(LED_GPIO_PORT, LED_GPIO_PINS);
  /* Turn off Led */
  PinLow(LED_GPIO_PORT, LED_GPIO_PINS);
}

void initClock(void)
{

  
  /* CLK configuration */
  //CLK_CKDIVR = (uint8_t)(0x00); // CLK_SYSCLKDiv_1 = 0x00
  /* Configures RTC clock */
  CLK_CRTCR = (uint8_t)((uint8_t)CLK_CRTCR_Source_LSE | (uint8_t)CLK_CLKDiv_1); // 
  /* Enable RTC */
  CLK_PCKENR2 |= (uint8_t)(1 << CLK_PCKENR2_RTC); 

}



