#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <stm8l.h>
#include <delay.h>
#include "gpio.h"
#include "uart.h"
#include "rtc.h"
#include "interrupt.h"


//#define LED_GPIO_PORT  PORTC
//#define LED_GPIO_PINS  PIN4

#define LED_GPIO_PORT  PORTB
#define LED_GPIO_PINS  PIN1


void initGPIO(void);
void initClock(void);
void initCalendar(void);

volatile uint8_t wuf = 0;
volatile uint8_t alarmf = 0;
rtc_TimeDef time;
rtc_DateDef date;

/*
 * Redirect stdout to UART
 */
int putchar(int c) {
    uart_write(c);
    return 0;
}

/*
 * Redirect stdin to UART
 */
int getchar() {
    return uart_read();
}


/* rtc_isr functions */
void rtc_isr() __interrupt(RTC_ISR) 
{
    if (rtc_getIT_WUT())
    {
        rtc_clearIT_WUT();
        //RTC_ISR2 = (uint8_t)~(uint8_t)(1<<RTC_ISR2_WUTF);
        //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
        wuf = 1;
    }
    
    if (rtc_getIT_ALRA())
    {
      rtc_clearIT_ALRA();
      alarmf = 1;
    }
}


void main(void) 
{
  uint8_t tick=0, sec;
  
  initClock();
  initGPIO();
  initCalendar();
  
  //SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  SYSCFG_RMPCR1 &= ~((1<<4) | (1<<5));
  SYSCFG_RMPCR1 |= (1<<4);  
  uart_init();
  

  rtc_wakeup_clock_config(0x04); // 0x04: RTC_WakeUpClock_CK_SPRE_16bits
  rtc_it_config();
  
  /* Wait until the calendar is synchronized */
  while (rtc_WaitForSynchro() == 0);
  time.Hours = 0;
  time.Minutes = 00;
  time.Seconds = 10;
  rtc_SetAlarmTime(&time, 2); // 2 : Tuesday

  rtc_ItConfig(0x10); // RTC_IT_ALRA : 0x10
  rtc_AlarmCmd(1);
  
  uart_write('\r');
  uart_write('\n');
  uart_write('-');
  uart_write('-');
  
  rtc_setWakeUpCounter(6); // 2 seconds
  rtc_wakeUpCmd(1);  
  
  enable_interrupts();

  while(1)  
  {
    /* CPU in Active Halt mode */
    __asm__("halt");
    
    if (alarmf)
    {
      alarmf = 0;      
      /* Wait until the calendar is synchronized */
      while (rtc_WaitForSynchro() == 0);
      rtc_GetTime(&time);
      
      uart_write('\r');
      uart_write('\n');
      uart_write('A');
      uart_write('L');
      uart_write('A');
      uart_write('R');
      uart_write('M');
      uart_write(' ');
      
      sec = time.Hours;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');

      sec = time.Minutes;
      uart_write(':');
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');      
      
      sec = time.Seconds;
      uart_write(':');
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');       
    }    
    else if (wuf)
    {
      wuf = 0;
      PinLow(LED_GPIO_PORT, LED_GPIO_PINS);
      
      /* Wait until the calendar is synchronized */
      while (rtc_WaitForSynchro() == 0);      
      rtc_GetTime(&time);
      
      uart_write('\r');
      uart_write('\n');
      
      sec = time.Hours;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');
      
      uart_write(':');
      sec = time.Minutes;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0'); 
      
      uart_write(':');
      sec = time.Seconds;
      uart_write((sec / 10) + '0');
      uart_write((sec % 10) + '0');

      PinHigh(LED_GPIO_PORT, LED_GPIO_PINS);  
      //printf("Test, %002d:%002d:%002d\r\n", time.Hours, time.Minutes, time.Seconds);
      //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
    }

  }
  

#if 0
    
  //enable_interrupts();
      
  while (1) 
  {
    /* CPU in Active Halt mode */
    __asm__("halt");

    if (wuf)
    {
      wuf = 0;
      tick++;
      
      if (tick==1)
      {
         PinLow(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==2)
      {
         PinHigh(LED_GPIO_PORT, LED_GPIO_PINS); 
      }
      else if (tick==5)
      {
        tick = 0;
        /* Wait until the calendar is synchronized */
        while (rtc_WaitForSynchro() == 0);        
        rtc_GetTime(&time);
        rtc_GetDate(&date);
      }
      
      /* Toggle LEDs LD1..LD4 */
      //PinToggle(LED_GPIO_PORT, LED_GPIO_PINS);
    }
  }
#endif  
}

/* ==================================================================== */
void initGPIO(void)
{
  /* 4.5uA with regulator(2uA), 2.5uA only stm8 */
  /* Input Pull Up all PORTs */
  //PA_DDR = 0;
  //PB_DDR = 0;
 //PC_DDR = 0;
  //PD_DDR = 0;
  /* Need to - 80uA */
  PA_CR1 = 0xFF;
  PB_CR1 = 0xFF;
  PC_CR1 = 0xFF;
  PD_CR1 = 0xFF;
  PinOutputFloat(PORTD, PIN0); // No Need
  //PinOutputFloat(PORTC, PIN5); // No Need
  //PinOutputFloat(PORTC, PIN6); // No Need
  // PinOutputFloat(PORTA, PIN2); => Increate current TXD
  //PinOutputFloat(PORTA, PIN0); // SWIM 2uA

  /* Config LED Port */
  PinOutput(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputPullUp(LED_GPIO_PORT, LED_GPIO_PINS);
  PinOutputSpeed10MHZ(LED_GPIO_PORT, LED_GPIO_PINS);
  /* Turn off Led */
  PinLow(LED_GPIO_PORT, LED_GPIO_PINS);
}

void initClock(void)
{
 
  /* CLK configuration */
  /* CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);  */
  //CLK_CKDIVR = (uint8_t)(0x00); // CLK_SYSCLKDiv_1 = 0x00
  CLK_CKDIVR = (uint8_t)(0x03); // CLK_SYSCLKDiv_8
  
  rtc_TurnOnLSE(1);
  /* Wait for LSERDY flag to be reset */
  while ((CLK_ECKCR & (1<<CLK_ECKCR_LSERDY)) == 0);

  /* Configures RTC clock */
  CLK_CRTCR = (uint8_t)((uint8_t)CLK_CRTCR_Source_LSE | (uint8_t)CLK_CLKDiv_1); // 
  /* Enable RTC */
  CLK_PCKENR2 |= (uint8_t)(1 << CLK_PCKENR2_RTC); 
}

void initCalendar(void)
{
  rtc_TimeDef time;
  rtc_DateDef date;
  
  //Calendar_Init()
  rtc_Init();
  
  date.DOW = 1;
  date.Day = 29;
  date.Month = 7;
  date.Year = 19;
  rtc_SetDate(&date);

  time.Hours = 23;
  time.Minutes = 59;
  time.Seconds = 50;
  rtc_SetTime(&time);
}
